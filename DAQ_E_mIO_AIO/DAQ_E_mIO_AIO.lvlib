﻿<?xml version='1.0' encoding='UTF-8'?>
<Library LVVersion="14008000">
	<Property Name="Alarm Database Computer" Type="Str">localhost</Property>
	<Property Name="Alarm Database Name" Type="Str">E__Program_Files_National_Instruments_LabVIEW_8_2_data</Property>
	<Property Name="Alarm Database Path" Type="Str">E:\Program Files\National Instruments\LabVIEW 8.2\data</Property>
	<Property Name="Data Lifespan" Type="UInt">3650</Property>
	<Property Name="Database Computer" Type="Str">localhost</Property>
	<Property Name="Database Name" Type="Str">E__Program_Files_National_Instruments_LabVIEW_8_2_data</Property>
	<Property Name="Database Path" Type="Str">E:\Program Files\National Instruments\LabVIEW 8.2\data</Property>
	<Property Name="Enable Alarms Logging" Type="Bool">false</Property>
	<Property Name="Enable Data Logging" Type="Bool">false</Property>
	<Property Name="NI.Lib.Description" Type="Str">This class implements the analog I/O functionality for Multi-IO cards from National Instruments that are base on the DAQmx driver. This class should work with all devices supported by the DAQmx driver and is not limited to E-Series devices.

author: Dietrich Beck, GSI
maintainer: Dietrich Beck, GSI

License Agreement for this software:

Copyright (C)
Gesellschaft für Schwerionenforschung, GSI
Planckstr. 1
64291 Darmstadt
Germany

Contact: D.Beck@gsi.de 

This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation; either version 2 of the license, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General License for more details (http://www.gnu.org).

Gesellschaft für Schwerionenforschung, GSI
Planckstr. 1, 64291 Darmstadt, Germany
For all questions and ideas contact: M.Richter@gsi.de, H.Brand@gsi.de or D.Beck@gsi.de.
Last update: 16-JUN-2004

INFO2SF
</Property>
	<Property Name="NI.Lib.HelpPath" Type="Str"></Property>
	<Property Name="NI.Lib.Icon" Type="Bin">&amp;!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!*P!!!*Q(C=\:9\EBJ"$):`8"MY.,[!893&lt;[AI=Q71&lt;+S=C*;(-&amp;22OSB(-&amp;81&amp;ALU!*^CK]4=^9PX9Y6'V&gt;J5$&gt;[.BZJ&gt;;_K;\[5*K\&lt;VUJ_Z;U]5W_,OB6X#H(Q_H;U8]&amp;+TOV@AW\,@AE3&amp;HRY]^X-4`XX_,@WR4`/)@OXPF8V\T,S`[FZ@^?#`\7]B:@Z^=:`WN&gt;B`UFP&gt;`A``+`)_V7X\?[[N"@\F*ETIDOM1#=]T;WX7*HOC*HOC*HOC"(OC"(OC"(OC/\OC/\OC/\OC'&lt;OC'&lt;OC'&lt;K@D!FXI1J?V_5_+*Y73J%G#*"A5*:?%*_&amp;*?")?(J8Q*$Q*4]+4]"#CB#@B38A3HI3(95JY%J[%*_&amp;*?%D63,*V&gt;(A3(N)LY!FY!J[!*_#BJ!+?!#!I&amp;C1/EI#BQ"H="$Q"4]$$L1+?A#@A#8A#(NQ+?!+?A#@A#8A9UG9F'EX@U?%BD2Q?B]@B=8A=(F,,Y8&amp;Y("[(R_'BH"Q?B]?"=!I[S5'1-]A*="Y=(I?(,TE]$I`$Y`!Y0,D;#HG&lt;G:[G\_DQ'$Q'D]&amp;D]"A]J*$"9`!90!;0Q5.;'4Q'D]&amp;D]"A]F*,"9`!90!;)5:4S-J):!YUA1T"Y_,4&gt;9GW6IJ&amp;9[`84($;K;A/K.J:KQ[AWAGK"61OH7B$62+MG5$5RKB&gt;7P9A+5&amp;69F6!6K#08!\&lt;(&gt;NA77W%,&lt;)\.M'E`^!]((I^((1Y(\@&gt;\\89\&lt;&lt;&gt;&lt;L69L,29,T?&gt;TT79T4;@4F^0K-XVIE^/ZN/:_]`$R[?O8_]@.YNPDJP^_O(`K^:PMU`.E`?(O&gt;OPD'4?=3``#W;BX,`_M;I[_!T-0&lt;KY!!!!!</Property>
	<Property Name="NI.Lib.SourceVersion" Type="Int">335577088</Property>
	<Property Name="NI.Lib.Version" Type="Str">1.16.0.0</Property>
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">true</Property>
	<Property Name="NI.SortType" Type="Int">3</Property>
	<Property Name="SaveStatePeriod" Type="UInt">0</Property>
	<Property Name="Serialized ACL" Type="Bin">&amp;!#!!!!!!!)!"1!&amp;!!!A1%!!!@````]!!".V&lt;H.J:WZF:#"C?82F)'&amp;S=G&amp;Z!!%!!1!!!!A)!!!!#!!!!!!!!!!</Property>
	<Property Name="Use Data Logging Database" Type="Bool">true</Property>
	<Item Name="public" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">1</Property>
		<Item Name="DAQ_E_mIO_AIO.constructor.vi" Type="VI" URL="../DAQ_E_mIO_AIO.constructor.vi"/>
		<Item Name="DAQ_E_mIO_AIO.destructor.vi" Type="VI" URL="../DAQ_E_mIO_AIO.destructor.vi"/>
		<Item Name="DAQ_E_mIO_AIO.get data to modify.vi" Type="VI" URL="../DAQ_E_mIO_AIO.get data to modify.vi"/>
		<Item Name="DAQ_E_mIO_AIO.get library version.vi" Type="VI" URL="../DAQ_E_mIO_AIO.get library version.vi"/>
		<Item Name="DAQ_E_mIO_AIO.set modified data.vi" Type="VI" URL="../DAQ_E_mIO_AIO.set modified data.vi"/>
		<Item Name="DAQ_E_mIO_AIO.emergency OFF.vi" Type="VI" URL="../DAQ_E_mIO_AIO.emergency OFF.vi"/>
		<Item Name="DAQ_E_mIO_AIO.get channel voltage.vi" Type="VI" URL="../DAQ_E_mIO_AIO.get channel voltage.vi"/>
		<Item Name="DAQ_E_mIO_AIO.set channel voltage.vi" Type="VI" URL="../DAQ_E_mIO_AIO.set channel voltage.vi"/>
	</Item>
	<Item Name="protected" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">1</Property>
		<Item Name="DAQ_E_mIO_AIO.get i attribute.vi" Type="VI" URL="../DAQ_E_mIO_AIO.get i attribute.vi"/>
		<Item Name="DAQ_E_mIO_AIO.set i attribute.vi" Type="VI" URL="../DAQ_E_mIO_AIO.set i attribute.vi"/>
		<Item Name="DAQ_E_mIO_AIO.ProcCases.vi" Type="VI" URL="../DAQ_E_mIO_AIO.ProcCases.vi"/>
		<Item Name="DAQ_E_mIO_AIO.ProcPeriodic.vi" Type="VI" URL="../DAQ_E_mIO_AIO.ProcPeriodic.vi"/>
	</Item>
	<Item Name="private" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">2</Property>
		<Item Name="DAQ_E_mIO_AIO.get AI range.vi" Type="VI" URL="../DAQ_E_mIO_AIO.get AI range.vi"/>
		<Item Name="DAQ_E_mIO_AIO.get AO range.vi" Type="VI" URL="../DAQ_E_mIO_AIO.get AO range.vi"/>
		<Item Name="DAQ_E_mIO_AIO.get device name.vi" Type="VI" URL="../DAQ_E_mIO_AIO.get device name.vi"/>
		<Item Name="DAQ_E_mIO_AIO.get logical names.vi" Type="VI" URL="../DAQ_E_mIO_AIO.get logical names.vi"/>
		<Item Name="DAQ_E_mIO_AIO.i attribute.ctl" Type="VI" URL="../DAQ_E_mIO_AIO.i attribute.ctl"/>
		<Item Name="DAQ_E_mIO_AIO.i attribute.vi" Type="VI" URL="../DAQ_E_mIO_AIO.i attribute.vi"/>
		<Item Name="DAQ_E_mIO_AIO.ProcEvents.vi" Type="VI" URL="../DAQ_E_mIO_AIO.ProcEvents.vi"/>
	</Item>
	<Item Name="inheritance" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">1</Property>
		<Item Name="PRIVATE.ProcCases.vi" Type="VI" URL="../inheritance/PRIVATE.ProcCases.vi"/>
		<Item Name="PUBLIC.constructor.vi" Type="VI" URL="../inheritance/PUBLIC.constructor.vi"/>
		<Item Name="PUBLIC.destructor.vi" Type="VI" URL="../inheritance/PUBLIC.destructor.vi"/>
	</Item>
	<Item Name="DAQ_E_mIO_AIO.contents.vi" Type="VI" URL="../DAQ_E_mIO_AIO.contents.vi"/>
	<Item Name="DAQ_E_mIO_AIO_db.ini" Type="Document" URL="../DAQ_E_mIO_AIO_db.ini"/>
	<Item Name="DAQ_E_mIO_AIO_mapping.ini" Type="Document" URL="../DAQ_E_mIO_AIO_mapping.ini"/>
	<Item Name="DAQ_E_mIO_AIO.gog" Type="Document" URL="../DAQ_E_mIO_AIO.gog"/>
</Library>
