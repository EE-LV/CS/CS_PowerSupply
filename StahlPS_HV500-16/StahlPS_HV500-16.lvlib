﻿<?xml version='1.0' encoding='UTF-8'?>
<Library LVVersion="14008000">
	<Property Name="NI.Lib.Icon" Type="Bin">&amp;!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!,C!!!*Q(C=T:3R&lt;B."%)&lt;`1S#:UC5&amp;5B38=4?PY#)PY)Z[2%G8.D1ILT!&gt;(:9FF!=)ACL&gt;.*&lt;CUC^!96%B+\)YPNOME25&lt;OQ("\MX?_@^X:\`&lt;8:^53E^[KP:9?8;Q(04&lt;QXZ\R$]W`F`\X?)=8Z^D21=,`M%RH8_`66&lt;XQ`PB9X^LZ(Z`&gt;DP$[^L6_GS^[^^3W^+OBJ4(`ARHVJ:W&gt;&lt;9_W`%X*OVKX&gt;P.PUF/O^&gt;P+VT(PS``^PPNY6NNFTX_=.W*N&gt;XDL_?&gt;N2[M@_-0\TJR5+Z^@DM(&lt;&amp;#O@@N(O:N,AY(GWCG&lt;PH&gt;T[K[NY_@H_0'\0(Z)`W[2GE*+*"'%%V&lt;?LEXU2%`U2%`U2!`U1!`U1!`U1(&gt;U2X&gt;U2X&gt;U2T&gt;U1T&gt;U1T&gt;U1S]68?B#&amp;TKLEES?4*1E42)EH5&amp;2UC1]#5`#E`$Q5QF0QJ0Q*$Q*$VW5]#1]#5`#E`!Q4!F0QJ0Q*$Q*$[E+3:;+$E`#1XI&amp;0!&amp;0Q"0Q"$R-K9!H!!AG#R)(3="19!90!5`!%`$QK)!HY!FY!J[!"VM"4]!4]!1]!1^$SKJ%I?EK/DSEE=0D]$A]$I`$1WIZ0![0Q_0Q/$R-*Y@(Y8%AH!G&gt;Z#$)'?2U=(YY0!Y0.TE]$I`$Y`!Y0&amp;BFB\SM4%@4682Y$"[$R_!R?!Q?5MDA-8A-(I0(Y#'N$"[$R_!R?!Q?JJ,"9`!90!;)-3H4SUBG$$1['9,"QV6/CZ6&gt;CE*CJ&gt;;`ZM."64W!KA&gt;,^=#I(A46$6&lt;&gt;/.5.56VIV16582D6&amp;V:^%66!V9F6%[JWV**W1&gt;Q15_++O#$'R)AY*@L&gt;U$`==&lt;F=;L&amp;9[/&lt;G2N0J6&amp;&gt;86\KYO."Y0.:I..,J[;H[`@[PL^5*^;%UG_`3*=`%=_*&amp;P4_/FVW@4&gt;]P\;JZLUFT`703@0Q_;;:@?]2Z]_(6J-4VZX/](HV[T?3EVXR[Q`W]6];^?[XG\&lt;=S8`0Q8@I@PIV[IH&lt;L-]];`13ZQRK(!!!!!!</Property>
	<Property Name="NI.Lib.SourceVersion" Type="Int">335577088</Property>
	<Property Name="NI.Lib.Version" Type="Str">1.0.0.0</Property>
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">true</Property>
	<Property Name="NI.SortType" Type="Int">3</Property>
	<Item Name="public" Type="Folder">
		<Item Name="StahlPS_HV500-16.constructor.vi" Type="VI" URL="../StahlPS_HV500-16.constructor.vi"/>
		<Item Name="StahlPS_HV500-16.get data to modify.vi" Type="VI" URL="../StahlPS_HV500-16.get data to modify.vi"/>
		<Item Name="StahlPS_HV500-16.set modified data.vi" Type="VI" URL="../StahlPS_HV500-16.set modified data.vi"/>
		<Item Name="StahlPS_HV500-16.reset.vi" Type="VI" URL="../StahlPS_HV500-16.reset.vi"/>
		<Item Name="StahlPS_HV500-16.emergency off.vi" Type="VI" URL="../StahlPS_HV500-16.emergency off.vi"/>
		<Item Name="StahlPS_HV500-16.set channel voltage.vi" Type="VI" URL="../StahlPS_HV500-16.set channel voltage.vi"/>
		<Item Name="StahlPS_HV500-16.get channel voltage.vi" Type="VI" URL="../StahlPS_HV500-16.get channel voltage.vi"/>
		<Item Name="StahlPS_HV500-16.set channel output.vi" Type="VI" URL="../StahlPS_HV500-16.set channel output.vi"/>
		<Item Name="StahlPS_HV500-16.get channel output.vi" Type="VI" URL="../StahlPS_HV500-16.get channel output.vi"/>
		<Item Name="StahlPS_HV500-16.get chassis temperature.vi" Type="VI" URL="../StahlPS_HV500-16.get chassis temperature.vi"/>
		<Item Name="StahlPS_HV500-16.get single channel voltage.vi" Type="VI" URL="../StahlPS_HV500-16.get single channel voltage.vi"/>
		<Item Name="StahlPS_HV500-16.destructor.vi" Type="VI" URL="../StahlPS_HV500-16.destructor.vi"/>
	</Item>
	<Item Name="protected" Type="Folder">
		<Item Name="StahlPS_HV500-16.set i attribute.vi" Type="VI" URL="../StahlPS_HV500-16.set i attribute.vi"/>
		<Item Name="StahlPS_HV500-16.get i attribute.vi" Type="VI" URL="../StahlPS_HV500-16.get i attribute.vi"/>
		<Item Name="StahlPS_HV500-16.ProcEvents.vi" Type="VI" URL="../StahlPS_HV500-16.ProcEvents.vi">
			<Property Name="NI.LibItem.Scope" Type="Int">2</Property>
		</Item>
		<Item Name="StahlPS_HV500-16.ProcCases.vi" Type="VI" URL="../StahlPS_HV500-16.ProcCases.vi"/>
		<Item Name="StahlPS_HV500-16.ProcCases Selector.vi" Type="VI" URL="../StahlPS_HV500-16.ProcCases Selector.vi"/>
		<Item Name="StahlPS_HV500-16.ProcPeriodic.vi" Type="VI" URL="../StahlPS_HV500-16.ProcPeriodic.vi"/>
		<Item Name="StahlPS_HV500-16.check range.vi" Type="VI" URL="../StahlPS_HV500-16.check range.vi"/>
		<Item Name="StahlPS_HV500-16.add dim services.vi" Type="VI" URL="../StahlPS_HV500-16.add dim services.vi"/>
		<Item Name="StahlPS_HV500-16.add mon services.vi" Type="VI" URL="../StahlPS_HV500-16.add mon services.vi"/>
		<Item Name="StahlPS_HV500-16.add set services.vi" Type="VI" URL="../StahlPS_HV500-16.add set services.vi"/>
		<Item Name="StahlPS_HV500-16.add setinfo services.vi" Type="VI" URL="../StahlPS_HV500-16.add setinfo services.vi"/>
		<Item Name="StahlPS_HV500-16.remove dim services.vi" Type="VI" URL="../StahlPS_HV500-16.remove dim services.vi"/>
		<Item Name="StahlPS_HV500-16.set single channel voltage.vi" Type="VI" URL="../StahlPS_HV500-16.set single channel voltage.vi"/>
		<Item Name="StahlPS_HV500-16.set multi channel voltage.vi" Type="VI" URL="../StahlPS_HV500-16.set multi channel voltage.vi"/>
		<Item Name="StahlPS_HV500-16.set single channel output.vi" Type="VI" URL="../StahlPS_HV500-16.set single channel output.vi"/>
		<Item Name="StahlPS_HV500-16.set multi channel output.vi" Type="VI" URL="../StahlPS_HV500-16.set multi channel output.vi"/>
		<Item Name="StahlPS_HV500-16.check id and attribute arrays.vi" Type="VI" URL="../StahlPS_HV500-16.check id and attribute arrays.vi"/>
		<Item Name="StahlPS_HV500-16.check id and enable arrays.vi" Type="VI" URL="../StahlPS_HV500-16.check id and enable arrays.vi"/>
		<Item Name="StahlPS_HV500-16.check id and voltage arrays.vi" Type="VI" URL="../StahlPS_HV500-16.check id and voltage arrays.vi"/>
		<Item Name="StahlPS_HV500-16.update all dim services.vi" Type="VI" URL="../StahlPS_HV500-16.update all dim services.vi"/>
		<Item Name="StahlPS_HV500-16.set default values.vi" Type="VI" URL="../StahlPS_HV500-16.set default values.vi"/>
		<Item Name="StahlPS_HV50016. set voltage PLC like.vi" Type="VI" URL="../StahlPS_HV50016. set voltage PLC like.vi"/>
	</Item>
	<Item Name="private" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">2</Property>
		<Item Name="StahlPS_HV500-16.i attribute.ctl" Type="VI" URL="../StahlPS_HV500-16.i attribute.ctl"/>
		<Item Name="StahlPS_HV500-16.i attribute.vi" Type="VI" URL="../StahlPS_HV500-16.i attribute.vi"/>
		<Item Name="StahlPS_HV500-16.ProcConstructor.vi" Type="VI" URL="../StahlPS_HV500-16.ProcConstructor.vi"/>
		<Item Name="StahlPS_HV500-16.ProcDestructor.vi" Type="VI" URL="../StahlPS_HV500-16.ProcDestructor.vi"/>
		<Item Name="StahlPS_HV500-16.get resource parameters.vi" Type="VI" URL="../StahlPS_HV500-16.get resource parameters.vi"/>
		<Item Name="StahlPS_HV500-16.channel not found.vi" Type="VI" URL="../StahlPS_HV500-16.channel not found.vi"/>
		<Item Name="StahlPS_HV500-16.evaluate overload status.vi" Type="VI" URL="../StahlPS_HV500-16.evaluate overload status.vi"/>
		<Item Name="StahlPS_HV500-16.set nominal channel voltage.vi" Type="VI" URL="../StahlPS_HV500-16.set nominal channel voltage.vi"/>
		<Item Name="StahlPS_HV500-16.set nominal channel output.vi" Type="VI" URL="../StahlPS_HV500-16.set nominal channel output.vi"/>
		<Item Name="StahlPS_HV500-16.dim service type.ctl" Type="VI" URL="../StahlPS_HV500-16.dim service type.ctl"/>
		<Item Name="StahlPS_HV500-16.get default values.vi" Type="VI" URL="../StahlPS_HV500-16.get default values.vi"/>
		<Item Name="StahlPS_HV500-16.dim service ids.ctl" Type="VI" URL="../StahlPS_HV500-16.dim service ids.ctl"/>
		<Item Name="StahlPS_HV500-16.get channel properties.vi" Type="VI" URL="../StahlPS_HV500-16.get channel properties.vi"/>
	</Item>
	<Item Name="StahlPS_HV500-16.contents.vi" Type="VI" URL="../StahlPS_HV500-16.contents.vi"/>
	<Item Name="StahlPS_HV500-16_db.ini" Type="Document" URL="../StahlPS_HV500-16_db.ini"/>
</Library>
